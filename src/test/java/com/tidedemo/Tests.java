/**
 * 
 */
package com.tidedemo;

import java.util.List;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import com.tide.demo.entities.UserIDFeature;
import com.tide.demo.entities.VersionFeature;
import com.tide.demo.rest.entities.ResponseFeatures;

/**
 * @author ajhidalgo
 *
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class Tests {
	
	private static final String FEATURE = "feature";
	private static final String VERSION = "version";
	private static final String USER_ID = "userID";
	private static final String BASE_URL = "http://localhost:8080/Demo/FeaturesService/";
	private static final String GET_ENABLED_FEATURES = "getEnabledFeatures/{version}/{userID}/";
	private static final String ENABLE_VERSION_FEATURE = "EnableVersionFeature/";
	private static final String DISABLE_VERSION_FEATURE = "DisableVersionFeature/{version}/{feature}/";
	private static final String ENABLE_USER_ID_FEATURE = "EnableUserIDFeature/";
	private static final String DISABLE_USER_ID_FEATURE = "DisableUserIDFeature/{userID}/{feature}/";
	
	private static String userID = "ajhidalgo";
	private static String version = "1";
	private static VersionFeature vf1 = new VersionFeature();
	private static VersionFeature vf2 = new VersionFeature();
	private static UserIDFeature uf1 = new UserIDFeature();
	private static UserIDFeature uf2 = new UserIDFeature();

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUp() throws Exception {
		// Set initial features.
		vf1.setVersion(version);
		vf1.setFeature("credit");
		vf2.setVersion(version);
		vf2.setFeature("xero");
		Response response1 = postVersionFeature(vf1);
		Response response2 = postVersionFeature(vf2);
		
		Assert.assertEquals(200, response1.getStatus());
		Assert.assertEquals(200, response2.getStatus());
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDown() throws Exception {
		// Delete features.
		Response response1 = deleteVersionFeature(vf2);
		Response response2 = deleteUserIDFeature(uf1);
		Response response3 = deleteUserIDFeature(uf2);
		
		Assert.assertEquals(200, response1.getStatus());
		Assert.assertEquals(200, response2.getStatus());		
		Assert.assertEquals(200, response3.getStatus());
	}

	/**
	 * User 'ajhidalgo' hasn't permissions.
	 */
	@Test
	public void test1() {
		List<String> result = getEnabledFeatures(version, userID);
		
		Assert.assertEquals(0, result.size());
	}

	/**
	 * User 'ajhidalgo' has permission on feature 'credit'.
	 */
	@Test
	public void test2() {
		// Insert permission
		uf1.setUserID(userID);
		uf1.setFeature("credit");
		Response response = postUserIDFeature(uf1);
		Assert.assertEquals(200, response.getStatus());
		
		List<String> result = getEnabledFeatures(version, userID);
		Assert.assertEquals(1, result.size());
		Assert.assertTrue(result.contains("credit"));
	}

	/**
	 * User 'ajhidalgo' has all permissions.
	 */
	@Test
	public void test3() {
		// Insert permission
		uf2.setUserID(userID);
		uf2.setFeature("xero");
		Response response = postUserIDFeature(uf2);
		Assert.assertEquals(200, response.getStatus());
		
		List<String> result = getEnabledFeatures(version, userID);
		Assert.assertEquals(2, result.size());
		Assert.assertTrue(result.contains("credit"));
		Assert.assertTrue(result.contains("xero"));
	}

	/**
	 * 'credit' is not longer working on this version.
	 * User 'ajhidalgo' has all permissions.
	 */
	@Test
	public void test4() {
		// Delete permission on version
		Response response = deleteVersionFeature(vf1);
		Assert.assertEquals(200, response.getStatus());
		
		List<String> result = getEnabledFeatures(version, userID);
		Assert.assertEquals(1, result.size());
		Assert.assertTrue(!result.contains("credit"));
		Assert.assertTrue(result.contains("xero"));
	}
	
	private List<String> getEnabledFeatures(String version, String userID) {
		Client client = ClientBuilder.newClient();
		WebTarget base = client.target(BASE_URL);
		WebTarget path = base.path(GET_ENABLED_FEATURES).resolveTemplate(VERSION, version).resolveTemplate(USER_ID, userID);
		ResponseFeatures result = path.request(MediaType.APPLICATION_JSON).get(ResponseFeatures.class);
		return result.getEnabledFeatures();
	}

	private static Response postVersionFeature(VersionFeature vf) {
		Client client = ClientBuilder.newClient();
		WebTarget base = client.target(BASE_URL);
		WebTarget path = base.path(ENABLE_VERSION_FEATURE);
		return path.request(MediaType.APPLICATION_JSON).post(Entity.json(vf));
	}
	
	private static Response postUserIDFeature(UserIDFeature uf) {
		Client client = ClientBuilder.newClient();
		WebTarget base = client.target(BASE_URL);
		WebTarget path = base.path(ENABLE_USER_ID_FEATURE);
		return path.request(MediaType.APPLICATION_JSON).post(Entity.json(uf));
	}
	
	private static Response deleteVersionFeature(VersionFeature vf) {
		Client client = ClientBuilder.newClient();
		WebTarget base = client.target(BASE_URL);
		WebTarget path = base.path(DISABLE_VERSION_FEATURE).resolveTemplate(VERSION, vf.getVersion()).resolveTemplate(FEATURE, vf.getFeature());
		return path.request(MediaType.APPLICATION_JSON).delete();
	}
	
	private static Response deleteUserIDFeature(UserIDFeature uf) {
		Client client = ClientBuilder.newClient();
		WebTarget base = client.target(BASE_URL);
		WebTarget path = base.path(DISABLE_USER_ID_FEATURE).resolveTemplate(USER_ID, uf.getUserID()).resolveTemplate(FEATURE, uf.getFeature());
		return path.request(MediaType.APPLICATION_JSON).delete();
	}

}
