/**
 * 
 */
package com.tide.demo.dao;

import java.util.List;

import com.tide.demo.entities.VersionFeature;

/**
 * @author ajhidalgo
 *
 */
public interface IVersionFeatureDao {
	
	public VersionFeature persist(VersionFeature entity);
	
	public VersionFeature update(VersionFeature entity);
	
	public void delete(VersionFeature entity);
	
	public VersionFeature findById(Long id);
	
	public List<VersionFeature> findAll();
	
	public List<String> findFeatureByVersion(String version);

	public VersionFeature findVersionFeature(VersionFeature vf);

}
