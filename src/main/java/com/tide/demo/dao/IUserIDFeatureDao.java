package com.tide.demo.dao;

import java.util.List;

import com.tide.demo.entities.UserIDFeature;

/**
 * 
 * @author ajhidalgo
 *
 */
public interface IUserIDFeatureDao {
	
	public UserIDFeature persist(UserIDFeature entity);
	
	public UserIDFeature update(UserIDFeature entity);
	
	public void delete(UserIDFeature entity);
	
	public UserIDFeature findById(Long id);
	
	public List<UserIDFeature> findAll();
	
	public List<String> findFeatureByUserID(String userID);

	public UserIDFeature findUserIDFeature(UserIDFeature uf);
}
