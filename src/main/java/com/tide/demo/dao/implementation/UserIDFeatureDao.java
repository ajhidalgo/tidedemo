/**
 * 
 */
package com.tide.demo.dao.implementation;

import java.util.LinkedList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.tide.demo.dao.IUserIDFeatureDao;
import com.tide.demo.entities.UserIDFeature;

/**
 * @author ajhidalgo
 *
 */
@Transactional
@Repository
public class UserIDFeatureDao extends GenericDAO<UserIDFeature, Long> implements IUserIDFeatureDao {

	@PersistenceContext
	private EntityManager em;

	public UserIDFeatureDao() {
		super(UserIDFeature.class, Long.class);
	}

	/* (non-Javadoc)
	 * @see com.tide.demo.dao.IUserIDFeature#findByUserID(java.lang.String)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<String> findFeatureByUserID(String userID) {
		List<String> resultList = new LinkedList<String>();
		try {
			List<UserIDFeature> response = em.createQuery("SELECT uf FROM UserIDFeature uf WHERE uf.userID LIKE ?1").setParameter(1, userID).getResultList();
			if (response != null) {
				for (UserIDFeature uf : response) {
					resultList.add(uf.getFeature());
				}
			}
			return resultList;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Override
	protected EntityManager getEntityManager() {
		return em;
	}

	@Override
	public UserIDFeature findUserIDFeature(UserIDFeature uf) {
		try {
			UserIDFeature result = (UserIDFeature) em.createQuery("SELECT uf FROM UserIDFeature uf WHERE uf.userID LIKE ?1 AND uf.feature LIKE ?2").setParameter(1, uf.getUserID()).setParameter(2, uf.getFeature()).getSingleResult();
			return result;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

}
