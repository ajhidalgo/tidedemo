/**
 * 
 */
package com.tide.demo.dao.implementation;

import java.io.Serializable;
import java.util.List;

import javax.naming.NamingException;
import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaQuery;

import org.springframework.transaction.annotation.Transactional;

/**
 * @author ajhidalgo
 *
 */
@Transactional
public abstract class GenericDAO<T, ID extends Serializable> {

	private Class<T> entityClass;

	public GenericDAO(Class<T> entityClass, Class<ID> idClass) {
		this.entityClass = entityClass;
	}

	protected abstract EntityManager getEntityManager();

	/**
	 * Persist the entity.
	 * 
	 * @param entity
	 * @throws NamingException
	 */
	public T persist(T entity) {
		getEntityManager().persist(entity);
		return entity;
	}

	/**
	 * Update the entity.
	 * 
	 * @param entity
	 * @throws NamingException
	 */
	public T update(T entity) {
		getEntityManager().merge(entity);
		getEntityManager().flush();
		return entity;
	}

	/**
	 * Delete the entity.
	 * 
	 * @param entity
	 * @throws NamingException
	 */
	public void delete(T entity) {
		getEntityManager().remove(getEntityManager().merge(entity));
		getEntityManager().flush();
	}

	/**
	 * Find an entity by ID.
	 * 
	 * @param id
	 * @return
	 * @throws NamingException
	 */
	public T findById(ID id) {
		return getEntityManager().find(entityClass, id);
	}

	/**
	 * Find all entities.
	 * 
	 * @return
	 * @throws NamingException
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public List<T> findAll() {
		CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
		cq.select(cq.from(entityClass));
		return getEntityManager().createQuery(cq).getResultList();
	}

}
