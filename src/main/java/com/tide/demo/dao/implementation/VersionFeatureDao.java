/**
 * 
 */
package com.tide.demo.dao.implementation;

import java.util.LinkedList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.tide.demo.dao.IVersionFeatureDao;
import com.tide.demo.entities.VersionFeature;

/**
 * @author ajhidalgo
 *
 */
@Transactional
@Repository
public class VersionFeatureDao extends GenericDAO<VersionFeature, Long> implements IVersionFeatureDao {

	@PersistenceContext
	private EntityManager em;

	public VersionFeatureDao() {
		super(VersionFeature.class, Long.class);
	}

	/* (non-Javadoc)
	 * @see com.tide.demo.dao.IVersionFeatureDao#findByVersion(java.lang.String)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<String> findFeatureByVersion(String version) {
		List<String> resultList = new LinkedList<String>();
		try {
			List<VersionFeature> response = em.createQuery("SELECT vf FROM VersionFeature vf WHERE vf.version LIKE ?1").setParameter(1, version).getResultList();
			if (response != null) {
				for (VersionFeature vf : response) {
					resultList.add(vf.getFeature());
				}
			}
			return resultList;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Override
	protected EntityManager getEntityManager() {
		return em;
	}

	@Override
	public VersionFeature findVersionFeature(VersionFeature vf) {
		try {
			VersionFeature result = (VersionFeature) em.createQuery("SELECT vf FROM VersionFeature vf WHERE vf.version LIKE ?1 AND vf.feature LIKE ?2").setParameter(1, vf.getVersion()).setParameter(2, vf.getFeature()).getSingleResult();
			return result;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

}
