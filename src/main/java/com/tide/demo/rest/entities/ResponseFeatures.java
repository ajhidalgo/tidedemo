/**
 * 
 */
package com.tide.demo.rest.entities;

import java.io.Serializable;
import java.util.List;

/**
 * @author ajhidalgo
 *
 */
public class ResponseFeatures implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ResponseFeatures() {
		super();
	}

	public ResponseFeatures(List<String> enabledFeatures) {
		this.enabledFeatures = enabledFeatures;
	}

	private List<String> enabledFeatures;

	public List<String> getEnabledFeatures() {
		return enabledFeatures;
	}

	public void setEnabledFeatures(List<String> enabledFeatures) {
		this.enabledFeatures = enabledFeatures;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((enabledFeatures == null) ? 0 : enabledFeatures.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ResponseFeatures other = (ResponseFeatures) obj;
		if (enabledFeatures == null) {
			if (other.enabledFeatures != null)
				return false;
		} else if (!enabledFeatures.equals(other.enabledFeatures))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "ResponseFeatures [enabledFeatures=" + enabledFeatures + "]";
	}
	
}
