/**
 * 
 */
package com.tide.demo.rest.config;

import javax.ws.rs.ApplicationPath;

import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.stereotype.Component;

import com.tide.demo.rest.FeaturesService;

/**
 * @author ajhidalgo
 *
 */
@Component
@ApplicationPath("Demo")
public class JerseyConfig extends ResourceConfig {

	public JerseyConfig() {
		register(FeaturesService.class);
	}

}
