/**
 * 
 */
package com.tide.demo.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.tide.demo.entities.UserIDFeature;
import com.tide.demo.entities.VersionFeature;
import com.tide.demo.manager.IFeaturesManager;
import com.tide.demo.rest.entities.ResponseFeatures;

/**
 * @author ajhidalgo
 *
 */
@Controller
@RequestMapping("FeaturesService")
public class FeaturesService {
	
	@Autowired
	private IFeaturesManager iFeaturesManager;
	
	@GetMapping("getEnabledFeatures/{version}/{userID}")
	public ResponseEntity<ResponseFeatures> getEnabledFeatures(@PathVariable("version") String version, @PathVariable("userID") String userID) {
		try {
			List<String> enabledFeatures = this.iFeaturesManager.findEnabledFeatures(version, userID);
			if (enabledFeatures != null) {
				ResponseFeatures response = new ResponseFeatures(enabledFeatures);
				return new ResponseEntity<ResponseFeatures>(response, HttpStatus.OK);
			} else {
				throw new Exception();
			}
		} catch (Exception e) {
			return new ResponseEntity<ResponseFeatures>(HttpStatus.CONFLICT);
		}
	}
	
	@GetMapping("getAllVersionFeature")
	public ResponseEntity<List<VersionFeature>> getAllVersionFeature() {
		try {
			List<VersionFeature> result = this.iFeaturesManager.findAllVersionFeature();
			if (result != null) {
				return new ResponseEntity<List<VersionFeature>>(result, HttpStatus.OK);
			} else {
				throw new Exception();
			}
		} catch (Exception e) {
			return new ResponseEntity<List<VersionFeature>>(HttpStatus.CONFLICT);
		}
	}
	
	@PostMapping("EnableVersionFeature")
	public ResponseEntity<Void> postVersionFeature(@RequestBody VersionFeature vf) {
		try {
			this.iFeaturesManager.persistVersionFeature(vf);
			return new ResponseEntity<Void>(HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<Void>(HttpStatus.CONFLICT);
		}
	}
	
	@DeleteMapping("DisableVersionFeature/{version}/{feature}")
	public ResponseEntity<Void> deleteVersionFeature(@PathVariable("version") String version, @PathVariable("feature") String feature) {
		try {
			VersionFeature vf = new VersionFeature();
			vf.setVersion(version);
			vf.setFeature(feature);
			this.iFeaturesManager.deleteVersionFeature(vf);
			return new ResponseEntity<Void>(HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<Void>(HttpStatus.CONFLICT);
		}
	}
	
	@GetMapping("getAllUserIDFeature")
	public ResponseEntity<List<UserIDFeature>> getAllUserIDFeature() {
		try {
			List<UserIDFeature> result = this.iFeaturesManager.findAllUserIDFeature();
			if (result != null) {
				return new ResponseEntity<List<UserIDFeature>>(result, HttpStatus.OK);
			} else {
				throw new Exception();
			}
		} catch (Exception e) {
			return new ResponseEntity<List<UserIDFeature>>(HttpStatus.CONFLICT);
		}
	}
	
	@PostMapping("EnableUserIDFeature")
	public ResponseEntity<Void> postUserIDFeature(@RequestBody UserIDFeature uf) {
		try {
			this.iFeaturesManager.persistUserIDFeature(uf);
			return new ResponseEntity<Void>(HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<Void>(HttpStatus.CONFLICT);
		}
	}
	
	@DeleteMapping("DisableUserIDFeature/{userID}/{feature}")
	public ResponseEntity<Void> deleteUserIDFeature(@PathVariable("userID") String userID, @PathVariable("feature") String feature) {
		try {
			UserIDFeature uf = new UserIDFeature();
			uf.setUserID(userID);
			uf.setFeature(feature);
			this.iFeaturesManager.deleteUserIDFeature(uf);
			return new ResponseEntity<Void>(HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<Void>(HttpStatus.CONFLICT);
		}
	}

}
