/**
 * 
 */
package com.tide.demo.manager;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tide.demo.dao.IUserIDFeatureDao;
import com.tide.demo.dao.IVersionFeatureDao;
import com.tide.demo.entities.UserIDFeature;
import com.tide.demo.entities.VersionFeature;

/**
 * @author ajhidalgo
 *
 */
@Service
public class FeaturesManager implements IFeaturesManager {
	
	@Autowired
	private IVersionFeatureDao iVersionFeatureDao;
	
	@Autowired
	private IUserIDFeatureDao iUserIDFeatureDao;

	/* (non-Javadoc)
	 * @see com.tide.demo.manager.IFeaturesManager#findEnabledFeatures(java.lang.String, java.lang.String)
	 */
	@Override
	public List<String> findEnabledFeatures(String version, String userID) {
		List<String> vfList = this.iVersionFeatureDao.findFeatureByVersion(version);
		List<String> ufList = this.iUserIDFeatureDao.findFeatureByUserID(userID);
		
		vfList.retainAll(ufList);
		
		return vfList;
	}
	
	@Override
	public void persistVersionFeature(VersionFeature vf) {
		this.iVersionFeatureDao.persist(vf);
	}

	@Override
	public void deleteVersionFeature(VersionFeature vf) {
		vf = this.iVersionFeatureDao.findVersionFeature(vf);
		this.iVersionFeatureDao.delete(vf);
	}

	@Override
	public void persistUserIDFeature(UserIDFeature uf) {
		this.iUserIDFeatureDao.persist(uf);
	}

	@Override
	public void deleteUserIDFeature(UserIDFeature uf) {
		uf = this.iUserIDFeatureDao.findUserIDFeature(uf);
		this.iUserIDFeatureDao.delete(uf);
	}

	@Override
	public List<VersionFeature> findAllVersionFeature() {
		return this.iVersionFeatureDao.findAll();
	}

	@Override
	public List<UserIDFeature> findAllUserIDFeature() {
		return this.iUserIDFeatureDao.findAll();
	}

}
