/**
 * 
 */
package com.tide.demo.manager;

import java.util.List;

import com.tide.demo.entities.UserIDFeature;
import com.tide.demo.entities.VersionFeature;

/**
 * @author ajhidalgo
 *
 */
public interface IFeaturesManager {
	
	public List<String> findEnabledFeatures(String version, String userID);
	
	public List<VersionFeature> findAllVersionFeature();
	
	public void persistVersionFeature(VersionFeature vf);
	
	public void deleteVersionFeature(VersionFeature vf);
	
	public List<UserIDFeature> findAllUserIDFeature();
	
	public void persistUserIDFeature(UserIDFeature uf);
	
	public void deleteUserIDFeature(UserIDFeature uf);

}
